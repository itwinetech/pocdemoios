using System;
using Foundation;
using UIKit;

namespace POCDEMO
{
	public partial class PocLandingScreenController : UIViewController
	{
		int rdbutton=0;
		string strRdbDatabase;
		internal static string strDBApi;
	
		public PocLandingScreenController (IntPtr handle) : base (handle)
		{
		}
		public override void ViewDidLoad()
		{
			base.ViewDidLoad ();
			RadioButtonSetUp ();
			FnClickEvents ();
		}

		void FnClickEvents()
		{
			btnLPLogin.TouchUpInside += delegate(object sender , EventArgs e )
			{
				if ( rdbutton == 0 )
				{
					CommonFunction.AlertDialog ( Constant.strAppName , Constant.strSelectDatabase );
				}
				else
				{				 
					if ( strRdbDatabase == "MYSQL" )
					{
						strDBApi = "http://202.83.22.139/pocwebapi";
					}
					else if ( strRdbDatabase == "ORACLE" )
					{
						strDBApi = "http://202.83.22.139/pocwebapioracle";
					}
					else
					{
						strDBApi = "http://202.83.22.139/pocwebapimssql";
					}
					PerformSegue ( "LoginSegue" , this );
				}
			};

			btnLPRegister.TouchUpInside += delegate(object sender , EventArgs e )
			{
				if ( rdbutton == 0 )
				{
					CommonFunction.AlertDialog ( Constant.strAppName , Constant.strSelectDatabase );
				}
				else
				{
					if ( strRdbDatabase == "MYSQL" )
					{
						strDBApi = "http://202.83.22.139/pocwebapi";
					}
					else if ( strRdbDatabase == "ORACLE" )
					{
						strDBApi = "http://202.83.22.139/pocwebapioracle";
					}
					else
					{
						strDBApi = "http://202.83.22.139/pocwebapimssql";
					}				
					PerformSegue ( "RegistrationSegue" , null );
				}
					
			};
		}

		//check box initial set up
		void RadioButtonSetUp()
		{
			//Radio Buttons
			btnMySql.Tag =10;
			btnOracle.Tag =20;
			btnMsSql.Tag =30;


			btnMySql.SetBackgroundImage (UIImage.FromBundle("Images/radiobuttonUnclick"),UIControlState.Normal);
			btnMySql.SetBackgroundImage (UIImage.FromBundle("Images/radiobuttonClick"),UIControlState.Selected);

			btnOracle.SetBackgroundImage (UIImage.FromBundle("Images/radiobuttonUnclick"),UIControlState.Normal);
			btnOracle.SetBackgroundImage (UIImage.FromBundle("Images/radiobuttonClick"),UIControlState.Selected);

			btnMsSql.SetBackgroundImage (UIImage.FromBundle("Images/radiobuttonUnclick"),UIControlState.Normal);
			btnMsSql.SetBackgroundImage (UIImage.FromBundle("Images/radiobuttonClick"),UIControlState.Selected);

			btnMySql.AddTarget (ButtonTapped,UIControlEvent.TouchUpInside);
			btnOracle.AddTarget (ButtonTapped,UIControlEvent.TouchUpInside);
			btnMsSql.AddTarget (ButtonTapped,UIControlEvent.TouchUpInside);
		}

		//method invoked when radio button clicked
		void ButtonTapped(object sender,EventArgs e)
		{
			strRdbDatabase = string.Empty;
			UIButton btnRadio = ( UIButton ) sender;

			if ( btnRadio.Tag == 10 || btnRadio.Tag == 20 || btnRadio.Tag == 30 )
			{
				btnMySql.Selected = false;
				btnOracle.Selected = false;
				btnMsSql.Selected = false;
			}
			if ( !btnRadio.Selected )
			{
				btnRadio.Selected = !btnRadio.Selected;
				switch ( btnRadio.Tag )
				{
					case 10:
						strRdbDatabase = "MYSQL";
						rdbutton = 1;
						break;
					case 20:
						strRdbDatabase = "ORACLE";
						rdbutton = 2;
						break;
					case 30:
						strRdbDatabase = "MSSQL";
						rdbutton = 3;
						break;
				}
			}
		}
	}
}
