// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace POCDEMO
{
	[Register ("LoginViewController")]
	partial class LoginViewController
	{
		[Outlet]
		UIKit.UIButton btnBack { get; set; }

		[Outlet]
		UIKit.UIButton btnImgRememberMe { get; set; }

		[Outlet]
		UIKit.UIButton btnNewMember { get; set; }

		[Outlet]
		UIKit.UIButton btnSignIn { get; set; }

		[Outlet]
		UIKit.UIButton tbnForgotPwd { get; set; }

		[Outlet]
		UIKit.UITextField txtLoginEmail { get; set; }

		[Outlet]
		UIKit.UITextField txtLoginPassword { get; set; }

		[Outlet]
		UIKit.UIView ViewLoginFields { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ViewLoginFields != null) {
				ViewLoginFields.Dispose ();
				ViewLoginFields = null;
			}

			if (txtLoginEmail != null) {
				txtLoginEmail.Dispose ();
				txtLoginEmail = null;
			}

			if (txtLoginPassword != null) {
				txtLoginPassword.Dispose ();
				txtLoginPassword = null;
			}

			if (btnSignIn != null) {
				btnSignIn.Dispose ();
				btnSignIn = null;
			}

			if (tbnForgotPwd != null) {
				tbnForgotPwd.Dispose ();
				tbnForgotPwd = null;
			}

			if (btnBack != null) {
				btnBack.Dispose ();
				btnBack = null;
			}

			if (btnNewMember != null) {
				btnNewMember.Dispose ();
				btnNewMember = null;
			}

			if (btnImgRememberMe != null) {
				btnImgRememberMe.Dispose ();
				btnImgRememberMe = null;
			}
		}
	}
}
