// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace POCDEMO
{
	[Register ("DemoViewController")]
	partial class DemoViewController
	{
		[Outlet]
		UIKit.UIButton btnAddCustomer { get; set; }

		[Outlet]
		UIKit.UIButton btnDatePicked { get; set; }

		[Outlet]
		UIKit.UIButton btnDelete { get; set; }

		[Outlet]
		UIKit.UIButton btnImageMenu { get; set; }

		[Outlet]
		UIKit.UIButton btnUpdate { get; set; }

		[Outlet]
		UIKit.UIDatePicker datePicker { get; set; }

		[Outlet]
		UIKit.UIView datePickerView { get; set; }

		[Outlet]
		UIKit.UIImageView imgUserImage { get; set; }

		[Outlet]
		UIKit.UILabel lblDates { get; set; }

		[Outlet]
		UIKit.UILabel lblUserName { get; set; }

		[Outlet]
		UIKit.UILabel lblViewHeaderName { get; set; }

		[Outlet]
		UIKit.UIScrollView scrollView { get; set; }

		[Outlet]
		UIKit.UITableView tblMenuList { get; set; }

		[Outlet]
		UIKit.UITableView tblViewAllUsers { get; set; }

		[Outlet]
		UIKit.UITextField txtAddressUpd { get; set; }

		[Outlet]
		UIKit.UITextField txtDOBUpd { get; set; }

		[Outlet]
		UIKit.UITextField txtEmailUpd { get; set; }

		[Outlet]
		UIKit.UITextField txtFNameUpd { get; set; }

		[Outlet]
		UIKit.UITextField txtLNameUpd { get; set; }

		[Outlet]
		UIKit.UITextField txtMobileUpd { get; set; }

		[Outlet]
		UIKit.UIView viewScroll { get; set; }

		[Outlet]
		UIKit.UIView viewSlideBar { get; set; }

		[Outlet]
		UIKit.UIView ViewUpdate { get; set; }

		[Action ("btnBack:")]
		partial void btnBack (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (lblViewHeaderName != null) {
				lblViewHeaderName.Dispose ();
				lblViewHeaderName = null;
			}

			if (btnAddCustomer != null) {
				btnAddCustomer.Dispose ();
				btnAddCustomer = null;
			}

			if (btnDatePicked != null) {
				btnDatePicked.Dispose ();
				btnDatePicked = null;
			}

			if (btnDelete != null) {
				btnDelete.Dispose ();
				btnDelete = null;
			}

			if (btnImageMenu != null) {
				btnImageMenu.Dispose ();
				btnImageMenu = null;
			}

			if (btnUpdate != null) {
				btnUpdate.Dispose ();
				btnUpdate = null;
			}

			if (datePicker != null) {
				datePicker.Dispose ();
				datePicker = null;
			}

			if (datePickerView != null) {
				datePickerView.Dispose ();
				datePickerView = null;
			}

			if (imgUserImage != null) {
				imgUserImage.Dispose ();
				imgUserImage = null;
			}

			if (lblDates != null) {
				lblDates.Dispose ();
				lblDates = null;
			}

			if (lblUserName != null) {
				lblUserName.Dispose ();
				lblUserName = null;
			}

			if (scrollView != null) {
				scrollView.Dispose ();
				scrollView = null;
			}

			if (tblMenuList != null) {
				tblMenuList.Dispose ();
				tblMenuList = null;
			}

			if (tblViewAllUsers != null) {
				tblViewAllUsers.Dispose ();
				tblViewAllUsers = null;
			}

			if (txtAddressUpd != null) {
				txtAddressUpd.Dispose ();
				txtAddressUpd = null;
			}

			if (txtDOBUpd != null) {
				txtDOBUpd.Dispose ();
				txtDOBUpd = null;
			}

			if (txtEmailUpd != null) {
				txtEmailUpd.Dispose ();
				txtEmailUpd = null;
			}

			if (txtFNameUpd != null) {
				txtFNameUpd.Dispose ();
				txtFNameUpd = null;
			}

			if (txtLNameUpd != null) {
				txtLNameUpd.Dispose ();
				txtLNameUpd = null;
			}

			if (txtMobileUpd != null) {
				txtMobileUpd.Dispose ();
				txtMobileUpd = null;
			}

			if (viewScroll != null) {
				viewScroll.Dispose ();
				viewScroll = null;
			}

			if (viewSlideBar != null) {
				viewSlideBar.Dispose ();
				viewSlideBar = null;
			}

			if (ViewUpdate != null) {
				ViewUpdate.Dispose ();
				ViewUpdate = null;
			}
		}
	}
}
