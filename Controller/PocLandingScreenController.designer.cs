// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace POCDEMO
{
	[Register ("PocLandingScreenController")]
	partial class PocLandingScreenController
	{
		[Outlet]
		UIKit.UIButton btnLPLogin { get; set; }

		[Outlet]
		UIKit.UIButton btnLPRegister { get; set; }

		[Outlet]
		UIKit.UIButton btnMsSql { get; set; }

		[Outlet]
		UIKit.UIButton btnMySql { get; set; }

		[Outlet]
		UIKit.UIButton btnOracle { get; set; }

		[Outlet]
		UIKit.UIImageView imgBtnLoginImage { get; set; }

		[Outlet]
		UIKit.UIImageView imgLandingImage { get; set; }

		[Outlet]
		UIKit.UIView viewContainsBtnLogin { get; set; }

		[Outlet]
		UIKit.UIView viewContainsbtnRegister { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnLPLogin != null) {
				btnLPLogin.Dispose ();
				btnLPLogin = null;
			}

			if (btnLPRegister != null) {
				btnLPRegister.Dispose ();
				btnLPRegister = null;
			}

			if (btnMySql != null) {
				btnMySql.Dispose ();
				btnMySql = null;
			}

			if (btnOracle != null) {
				btnOracle.Dispose ();
				btnOracle = null;
			}

			if (btnMsSql != null) {
				btnMsSql.Dispose ();
				btnMsSql = null;
			}

			if (imgBtnLoginImage != null) {
				imgBtnLoginImage.Dispose ();
				imgBtnLoginImage = null;
			}

			if (imgLandingImage != null) {
				imgLandingImage.Dispose ();
				imgLandingImage = null;
			}

			if (viewContainsBtnLogin != null) {
				viewContainsBtnLogin.Dispose ();
				viewContainsBtnLogin = null;
			}

			if (viewContainsbtnRegister != null) {
				viewContainsbtnRegister.Dispose ();
				viewContainsbtnRegister = null;
			}
		}
	}
}
