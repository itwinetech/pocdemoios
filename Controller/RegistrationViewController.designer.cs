// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace POCDEMO
{
	[Register ("RegistrationViewController")]
	partial class RegistrationViewController
	{
		[Outlet]
		UIKit.UIButton btnAlreadyMember { get; set; }

		[Outlet]
		UIKit.UIButton btnChkBoxTnC { get; set; }

		[Outlet]
		UIKit.UIButton btnRegister { get; set; }

		[Outlet]
		UIKit.UIImageView imgIconEmail { get; set; }

		[Outlet]
		UIKit.UIImageView imgIconFullName { get; set; }

		[Outlet]
		UIKit.UIImageView imgIconPassword { get; set; }

		[Outlet]
		UIKit.UIImageView imgIconRetypePassword { get; set; }

		[Outlet]
		UIKit.UITextField txtEmail { get; set; }

		[Outlet]
		UIKit.UITextField txtFirstName { get; set; }

		[Outlet]
		UIKit.UITextField txtLastName { get; set; }

		[Outlet]
		UIKit.UITextField txtPassword { get; set; }

		[Outlet]
		UIKit.UITextField txtReTypePassword { get; set; }

		[Outlet]
		UIKit.UIView viewRegistrationFields { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgIconEmail != null) {
				imgIconEmail.Dispose ();
				imgIconEmail = null;
			}

			if (imgIconFullName != null) {
				imgIconFullName.Dispose ();
				imgIconFullName = null;
			}

			if (imgIconPassword != null) {
				imgIconPassword.Dispose ();
				imgIconPassword = null;
			}

			if (imgIconRetypePassword != null) {
				imgIconRetypePassword.Dispose ();
				imgIconRetypePassword = null;
			}

			if (viewRegistrationFields != null) {
				viewRegistrationFields.Dispose ();
				viewRegistrationFields = null;
			}

			if (txtFirstName != null) {
				txtFirstName.Dispose ();
				txtFirstName = null;
			}

			if (txtLastName != null) {
				txtLastName.Dispose ();
				txtLastName = null;
			}

			if (txtEmail != null) {
				txtEmail.Dispose ();
				txtEmail = null;
			}

			if (txtPassword != null) {
				txtPassword.Dispose ();
				txtPassword = null;
			}

			if (txtReTypePassword != null) {
				txtReTypePassword.Dispose ();
				txtReTypePassword = null;
			}

			if (btnAlreadyMember != null) {
				btnAlreadyMember.Dispose ();
				btnAlreadyMember = null;
			}

			if (btnChkBoxTnC != null) {
				btnChkBoxTnC.Dispose ();
				btnChkBoxTnC = null;
			}

			if (btnRegister != null) {
				btnRegister.Dispose ();
				btnRegister = null;
			}
		}
	}
}
