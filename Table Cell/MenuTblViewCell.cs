﻿using System;

using Foundation;
using UIKit;

namespace POCDEMO
{
	public partial class MenuTblViewCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString ( "MenuTblViewCell" );

		public static readonly UINib Nib;

		static MenuTblViewCell ()
		{
			Nib = UINib.FromName ( "MenuTblViewCell" , NSBundle.MainBundle );
		}

		public MenuTblViewCell ( IntPtr handle ) : base ( handle )
		{
		}
		public static MenuTblViewCell Create ()
		{
			return ( MenuTblViewCell ) Nib.Instantiate ( null , null )[0] ;
		}
		internal void BindData(string strLabel,string strIconPath)
		{ 
			lblMenuText.Text =strLabel;
			imgViewMenuIcon.Image = UIImage.FromBundle ( strIconPath );

		}
	}
}
