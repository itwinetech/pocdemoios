﻿using System;

using Foundation;
using UIKit;
using POC.EntityModel;

namespace POCDEMO
{
	public partial class ListCustomerTblViewCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString ( "ListCustomerTblViewCell" );

		public static readonly UINib Nib;

		static ListCustomerTblViewCell ()
		{
			Nib = UINib.FromName ( "ListCustomerTblViewCell" , NSBundle.MainBundle );
		}

		public ListCustomerTblViewCell ( IntPtr handle ) : base ( handle )
		{
		}
		public static ListCustomerTblViewCell Create ()
		{
			return ( ListCustomerTblViewCell ) Nib.Instantiate ( null , null )[0] ;
		}
		internal void BindData(CustomerModel objCustomerModel )
		{ 
			lblCellCustName.Text =objCustomerModel.FirstName +" "+objCustomerModel.LastName;

		}
	}
}
