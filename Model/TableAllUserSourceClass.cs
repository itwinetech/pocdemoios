﻿using System;
using UIKit;
using POC.EntityModel;
using System.Collections.Generic;
using Foundation;

namespace POCDEMO
{
	public class TableAllUserSourceClass :UITableViewSource
	{
//		public TableAllUserSourceClass ()
//		{
//		}
//		string CellIdentifier = "TableCell";

		List<CustomerModel>  TableItems; 
		public event Action<CustomerModel> RowTaped;
		public TableAllUserSourceClass(List<CustomerModel> items)
		{
			TableItems = items;
		}
				public override nint RowsInSection(UITableView tableView,nint section)
		{
			return TableItems.Count;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			ListCustomerTblViewCell cell=tableView.DequeueReusableCell ( ListCustomerTblViewCell.Key ) as ListCustomerTblViewCell ?? ListCustomerTblViewCell.Create ();
			cell.BindData ( TableItems [indexPath.Row] );
			return cell;
		}
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			if ( RowTaped != null )
				RowTaped (TableItems[indexPath.Row]);

			tableView.DeselectRow (indexPath,true);
		}
	}
}

 