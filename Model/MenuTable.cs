﻿ using System;
using UIKit;

namespace POCDEMO
{
	public class MenuTable :UITableViewSource
	{
		readonly string[] arrMenuText;
		readonly string[] arrMenuIcon;
		internal event Action<string> MenuSelected;

		public MenuTable (string[] strArrayName,string[] strArrayImageIcon)
		{
			arrMenuText=strArrayName;
			arrMenuIcon = strArrayImageIcon;
		}
		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return arrMenuText.Length;
		}

		public override UITableViewCell GetCell (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			MenuTblViewCell cell = tableView.DequeueReusableCell ( MenuTblViewCell.Key ) as MenuTblViewCell ?? MenuTblViewCell.Create ();
			cell.BindData (arrMenuText[indexPath.Row],arrMenuIcon[indexPath.Row]);
			return cell;
		}

		public override void RowSelected (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			if ( MenuSelected != null )
			MenuSelected (arrMenuText[indexPath.Row]);
			tableView.DeselectRow (indexPath,true);
		}
	}
}

