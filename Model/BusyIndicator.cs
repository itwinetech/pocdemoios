﻿using System;
using UIKit;
using CoreGraphics;

namespace POCDEMO
{
	public sealed class BusyIndicator:UIView
	{
		readonly UIActivityIndicatorView spinner;
		readonly UILabel lblLoading;
		public BusyIndicator (CGRect frame,string strMsg):base(frame)
		{
			BackgroundColor = UIColor.Black;
			Alpha = 0.75f;

			const float labelHeight = 22;

			float flWidth = Convert.ToSingle (Frame.Width.ToString());
			float flHeight = Convert.ToSingle (Frame.Height.ToString());

			float labelWidth =  flWidth- 20;
			float centerX = flWidth / 2;
			float centerY = flHeight / 2;

			spinner = new UIActivityIndicatorView ( UIActivityIndicatorViewStyle.WhiteLarge );

			spinner.Frame = new CGRect ( centerX - ( spinner.Frame.Width / 2 ) , centerY - spinner.Frame.Height - 20 , spinner.Frame.Width , spinner.Frame.Height );
			spinner.AutoresizingMask = UIViewAutoresizing.FlexibleMargins;
			AddSubview ( spinner );
			spinner.StartAnimating ();

			lblLoading = new UILabel ( new CGRect (centerX - ( labelWidth / 2 ) , centerY + 20 , labelWidth , labelHeight ) );
			lblLoading.BackgroundColor = UIColor.Clear;
			lblLoading.TextColor = UIColor.White;
			lblLoading.Text = strMsg;
			lblLoading.TextAlignment = UITextAlignment.Center;
			lblLoading.AutoresizingMask = UIViewAutoresizing.FlexibleMargins;
			AddSubview ( lblLoading );
		}

		public void Hide ()
		{
			RemoveFromSuperview ();
		}
	}
}

