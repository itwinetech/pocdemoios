﻿using System;
using System.Text.RegularExpressions;

using System.IO; 
using UIKit;


namespace POCDEMO
{
	public class CommonFunction
	{
		const string strEmailPattern=@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
			@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
				 
		const string strPasswordPattern =@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$";

		const string date_regex = @"^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$" ;

		const string strPhonePattern=@"^[1-9]{1}[0-9]{9}$";

		const string strOnlyAlphabet = @"^(a-zA-Z)&";



		internal static bool fnAlphabetValidation(string strFLName)
		{
			Regex rex = new Regex ( strOnlyAlphabet );
			return rex.IsMatch ( strFLName );
		}

		internal static bool fnFieldValidation(string strEmail,string strPassword)
		{ 
			bool isSuccess=false;
			if ( string.IsNullOrEmpty ( strEmail ) && string.IsNullOrEmpty ( strPassword) )
			{
				isSuccess = true;
			}
			return isSuccess;

		}
			
		internal static bool fnPhoneValidation(string strPhNumber)
		{
			Regex rex = new Regex ( strPhonePattern );
			return rex.IsMatch ( strPhNumber );
		}
		internal static bool fnEmailValidation(string strMail)
		{
			Regex rex = new Regex (strEmailPattern);
			return rex.IsMatch (strMail);
		}

		internal static bool fnDateFormatValidation(string strdate)
		{
			Regex rex = new Regex (date_regex);
			return rex.IsMatch (strdate);
		}

		internal static bool fnPasswordValidation(string strPassword)
		{
			Regex rex = new Regex (strPasswordPattern);
			return rex.IsMatch (strPassword);
		}

		internal static void AlertDialog(string strTitle,string strMessage )
		{
			UIAlertView alertView=new UIAlertView ();
			alertView.Title = strTitle;
			alertView.Message = strMessage;
			alertView.AddButton ("Ok");
			alertView.Show ();
		}

		internal static bool  GetAge(DateTime Dob)
		{
			//Console.WriteLine (FullAge(Dob));
			bool isTrue = false;
			try
			{
				int intAgeInYear=DateTime.Today.Year-Dob.Year;
				if(Dob>DateTime.Today.AddYears(-intAgeInYear))
				{
					intAgeInYear--;
				}
				if(intAgeInYear>=18)
					isTrue=true;
			}
			catch
			{
				isTrue = false;
			}

			return isTrue;
		}


	}
}


