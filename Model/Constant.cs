﻿using System;

namespace POCDEMO
{
	public class Constant
	{
		internal static readonly string strAppName="Demo Poc";
		internal static readonly string strNoInternet="No internet connection. Make sure your network connectivity in active and try again.";
		internal static readonly string strEnterAllFields="You must fill in all of the fields.";
		internal static readonly string strValidEmail="Invalid mail id";
		internal static readonly string strLoginFailed="Login Faild";
		internal static readonly string strPassWordNotMatch="Password does not match the confirm password";
		internal static readonly string strValidMobileNumber="Please provide 10 digit MobileNo ";
		internal static readonly string strRegisteredUnSuccessfully="Customer Details Registered UnSuccessfully";
		internal static readonly string strRegisteredSuccessfully="Customer Details Registered Successfully";
		public const string strCustomerUpdateSuccessMsg ="Customer Details Updated";
		public const string strCustomerUpdateUnSuccessMsg="Unsuccessfull Updated";

		internal static readonly string strSaveCustomerDataSuccess="Customer Details Saved Successfully";
		internal static readonly string strSaveCustomerDataFailed="Customer Details Saved UnSuccessfully";
		internal static readonly string strSelectDatabase="Please select database.";

		internal static readonly string strValidName="Please Provide Alphabet to your name";
		internal static readonly string strIsAgreed="Please agree the terms";
		internal static readonly string strValidDOB="Please provide Proper DOB format!";
		internal static readonly string strDeletedCustomerSuccessMsg="Customer deleted successfully";
		internal static readonly string strDeletedCustomerUnSuccessMsg="Could not able to delete customer!";


		internal static readonly string strDeleteConfirmation="Are you sure want to delete customer!";

	}
}

