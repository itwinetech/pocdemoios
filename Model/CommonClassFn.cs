﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
namespace POCDEMO
{
	public static class CommonClassFn
	{
		#region declarations
		const string passPhrase = "itwinetech";                // can be any string 
		const string saltValue = "s@1tValue";             // can be any string
		const string hashAlgorithm = "MD5";               // can be "MD5" or "SHA1"
		const int passwordIterations = 2;              // can be any number
		const string initVector = "@1B2c3D4e5F6g7H8";       // must be 16 bytes
		const int keySize = 256;                        // can be 192 or 128 or 256
		#endregion

		#region Private Methods
		private static string Encrypt(string plainText, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
		{
			byte[] initVectorBytes = null;
			byte[] saltValueBytes = null;
			byte[] plainTextBytes = null;
			byte[] keyBytes = null;
			byte[] cipherTextBytes = null;
			initVectorBytes = Encoding.ASCII.GetBytes(initVector);
			saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
			plainTextBytes = Encoding.UTF8.GetBytes(plainText);
			PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
			keyBytes = password.GetBytes(keySize / 8);
			RijndaelManaged symmetricKey = new RijndaelManaged();
			symmetricKey.Mode = CipherMode.CBC;
			ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
			MemoryStream memoryStream = new MemoryStream();
			CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
			cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
			cryptoStream.FlushFinalBlock();
			cipherTextBytes = memoryStream.ToArray();
			memoryStream.Close();
			cryptoStream.Close();
			string cipherText = Convert.ToBase64String(cipherTextBytes);
			return cipherText;
		}

		private static string Decrypt(string cipherText, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
		{
			byte[] initVectorBytes = null;
			byte[] saltValueBytes = null;
			byte[] cipherTextBytes = null;
			byte[] keyBytes = null;
			byte[] plainTextBytes = null;
			int decryptedByteCount = 0;
			string plainText = string.Empty;
			initVectorBytes = Encoding.ASCII.GetBytes(initVector);
			saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
			cipherTextBytes = Convert.FromBase64String(cipherText.Replace(" ", "+"));
			PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
			keyBytes = password.GetBytes(keySize / 8);
			RijndaelManaged symmetricKey = new RijndaelManaged();
			symmetricKey.Mode = CipherMode.CBC;
			ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
			MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
			CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
			plainTextBytes = new byte[cipherTextBytes.Length + 1];
			decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
			memoryStream.Close();
			cryptoStream.Close();
			plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
			return plainText;
		}
		#endregion

		#region Public Methods
		// utilty function to convert string to byte[]   
		public static byte[] GetBytes(string str)
		{
			byte[] bytes = new byte[str.Length * sizeof(char)];
			System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}

		// utilty function to convert byte[] to string        
		public static string GetString(byte[] bytes)
		{
			char[] chars = new char[bytes.Length / sizeof(char)];
			System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
			return new string(chars);
		}

		/// <summary>
		/// This function encrypts the data.
		/// </summary>
		/// <param name="strData">The string value to be encrypted.</param> 
		public static string EncryptData(string strData)
		{
			string plainText = string.Empty;
			string cipherText = string.Empty;

			plainText = strData;
			cipherText = CommonClassFn.Encrypt(plainText, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);

			return cipherText;

		}

		/// <summary>
		/// This function Decrypts the data.
		/// </summary>
		/// <param name="strData">The string value to be decrypted.</param> 
		public static string DecryptData(string strData)
		{

			string plainText = string.Empty;
			string tempcipherText = string.Empty;
			tempcipherText = strData;
			plainText = CommonClassFn.Decrypt(tempcipherText, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
			return plainText;

		}

		/// <summary>
		/// Method to Generate a DatePrefix for E-book and Preview Book
		/// </summary> 
		public static string CreatePrefix()
		{
			string DatePrefix = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
			DatePrefix = DatePrefix.Remove(2, 1);
			DatePrefix = DatePrefix.Remove(4, 1);
			DatePrefix = DatePrefix.Remove(8, 1);
			DatePrefix = DatePrefix.Remove(10, 1);
			DatePrefix = DatePrefix.Remove(12, 1);
			return DatePrefix;
		}

		/// <summary>
		/// To generate Random string of required legth
		/// </summary>
		/// <param name="intLength">Required Length in integer</param>
		/// <returns> String of given Length</returns>
		public static string RandomString(int intLength)
		{
			char[] chrCharecters = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 
				'g', 'h', 'i', 'j', 'k', 'l', 
				'm', 'n', 'o', 'p', 'q', 'r', 
				's', 't', 'u', 'v', 'w', 'x', 
				'y', 'z', 'A', 'B', 'C', 'D', 
				'E', 'F', 'G', 'H', 'I', 'J', 
				'K', 'L', 'M', 'N', 'O', 'P', 
				'Q', 'R', 'S', 'T', 'U', 'V', 
				'W', 'X', 'Y', 'Z', '0', '1', 
				'2', '3', '4', '5', '6', '7', 
				'8', '9'};
			Random objRandam = new Random();
			string strRamdom = string.Empty;
			for (int cnt = 0; cnt <= intLength - 1; cnt++)
			{
				strRamdom += chrCharecters[objRandam.Next(0, 61)];
			}
			strRamdom = strRamdom.Replace("'", string.Empty);
			return strRamdom;
		}

		/// <summary>
		/// Method is to check the value is decimal (12,2)
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool ValidateDecimal(string value)
		{
			Regex r = new Regex(@"^-{0,1}\d*\.{0,1}\d{0,2}$");
			if (r.IsMatch(value))
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// Method is to check the value is Alphanumeric or not
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool ValidateAlphanumeric(string value)
		{
			Regex r = new Regex("^[a-zA-Z0-9]*$");
			if (r.IsMatch(value))
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// Method is to check the date for dd/MM/yyyy format
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool ValidateDate(string value)
		{
			Regex r = new Regex(@"^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$");
			if (r.IsMatch(value))
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// Method is to check the value is Email or not
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool ValidateEmail(string value)
		{
			Regex r = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
			if (r.IsMatch(value))
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// Method is to check the Name
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool ValidateName(string value)
		{
			Regex r = new Regex(@"^[A-Za-z& - ]+$");
			if (r.IsMatch(value))
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// Method is to check the ZipCode
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool ValidateZipCode(string value)
		{
			Regex r = new Regex(@"^[0-9]{4,6}$");
			if (r.IsMatch(value))
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// Method is to check the Phone
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool ValidatePhone(string value)
		{
			Regex r = new Regex(@"^[+][0-9]{2}[-][0-9]{3}[-][0-9]{4}$");
			if (r.IsMatch(value))
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// Method is to check the value is numeric or not
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool ValidateNumber(string value)
		{
			Regex r = new Regex("^[0-9]$");
			if (r.IsMatch(value))
			{
				return true;
			}
			return false;
		}
		#endregion

	}
}


